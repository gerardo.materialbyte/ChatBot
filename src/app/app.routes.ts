import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ChatDialogComponent } from './components/chat/chat-dialog/chat-dialog.component';
import { AcercaDeComponent } from './components/acerca-de/acerca-de.component';

const APP_ROUTES: Routes = [
   { path: 'acerca-de', component: AcercaDeComponent },
   { path: 'home', component: HomeComponent },
   { path: 'chat', component: ChatDialogComponent },
   { path: '**', pathMatch: 'full', redirectTo: 'home'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
