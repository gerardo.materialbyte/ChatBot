import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
//servicios
//rutas
import { APP_ROUTING } from './app.routes'

import { AppComponent } from './app.component';
import { ChatModule } from './components/chat/chat.module';

//componentes
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { ChatDialogComponent } from './components/chat/chat-dialog/chat-dialog.component';
import { AcercaDeComponent } from './components/acerca-de/acerca-de.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    AcercaDeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ChatModule,
    APP_ROUTING
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
